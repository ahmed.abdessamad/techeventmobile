/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techevent.Services;

import GUI.HomeForm;
import com.codename1.components.SpanLabel;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Form;
import com.codename1.ui.events.ActionListener;
import com.techevent.Services.ServiceEvent;
import com.techevent.entities.AppelSponsor;
import com.techevent.entities.Event;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author JDidi-PC
 */
public class ServiceAppelSponsor {
   
    
    
     ArrayList<AppelSponsor> listEvent = new ArrayList<>();
       ArrayList<AppelSponsor> mesEvent = new ArrayList<>();
    
    
    public void ajoutEvent(Event a) {
        ConnectionRequest con = new ConnectionRequest();// créat&ne nouvelle demande de connexion
        String Url = "http://localhost/techeventmobile/web/app_dev.php/Ajouterevent?titre="+a.getTitre()+"&description="+ a.getDescription()+"&nbrpalce="+ a.getNbrplaces()+"&localisation="+ a.getLocalisation()+"&dateevent="+ a.getDateevent()+"&hdebut="+ a.getHdebut()+"&hfin="+ a.getHfin()+
                "&prix="+ a.getPrix()+"&categories="+ a.getCategorie()+"&type="+ a.getType()+"";// création de l'URL
        con.setUrl(Url);// Insertion de l'URL de notre demande de connexion

        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());//Récupération de la réponse du serveur
            System.out.println(str);//Affichage de la réponse serveur sur la console

        });
        NetworkManager.getInstance().addToQueueAndWait(con);// Ajout de notre demande de connexion à la file d'attente du NetworkManager
    }
    
    public ArrayList<AppelSponsor> getListInvitations(){       
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/techeventsweb/web/app_dev.php/aaaaa/invitationC");  
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                ServiceAppelSponsor ser = new ServiceAppelSponsor();
                listEvent =ser.getListInvi(new String(con.getResponseData()));
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listEvent;
    }
    
    
     public ArrayList<AppelSponsor> getListInvi(String json) {

        ArrayList<AppelSponsor> listDemandes = new ArrayList<>();

        try {
            System.out.println("this is json latrab7ou ");
            System.out.println(json);
            JSONParser j = new JSONParser();

            Map<String, Object> lesDemandes = j.parseJSON(new CharArrayReader(json.toCharArray()));
            
           
            List<Map<String, Object>> list = (List<Map<String, Object>>) lesDemandes.get("root");

            for (Map<String, Object> obj : list) {
                AppelSponsor ev = new AppelSponsor();

                       
                
               // String a = obj.get("nomsponsor").toString();
                
                
                
              //  float id = Float.parseFloat(obj.get("id").toString());
//                 float prix = Float.parseFloat(obj.get("prix").toString());
                   // float iduser = Float.parseFloat(obj.get("User_id").toString());
             //  ev.setId((int) id);
               // ev.setPrix((int) prix);
                //ev.setUser_id((int) iduser);
             
            //    ev.setId(i);
                ev.setNomevent(obj.get("nomevent").toString());
             //    ev.setPrix(obj.get("prix").toString());
            //  ev.setNomSponsor(obj.get("nomsponsor").toString());
              //
              //ev.setNbrplaces(obj.get("nbrplaces").toString());
              //ev.setLocalisation(obj.get("localisation").toString());
               ev.setDateevent(obj.get("dateevent").toString());
               //
               
              //  ev.setHdebut(obj.get("hdebut").toString());
              //  ev.setHfin(obj.get("hfin").toString());
                //
               //  ev.setCategorie(obj.get("categorie").toString());
               // ev.setType(obj.get("type").toString());
               
//                       System.out.println(obj.get("dateevent").toString());
//                       System.out.println(obj.get("nomsponsor").toString());
//                       System.out.println(obj.get("prix").toString());
//                       System.out.println(obj.get("id").toString());
              

                
                
                listDemandes.add(ev);

            }

        } catch (IOException ex) {
        }
       
        return listDemandes;

     }
     
    
     
     Form f;
    SpanLabel lb;
  
     public void supprimerEvent(int a){
          ConnectionRequest con = new ConnectionRequest();

     
        con.setUrl("http://localhost/techeventmobile/web/app_dev.php/deleteevent/"+a+"");  
       
          
        NetworkManager.getInstance().addToQueueAndWait(con);
        
        
          f = new Form();
        lb = new SpanLabel("");
        f.add(lb);
        ServiceEvent serviceTask =new ServiceEvent();
        lb.setText(serviceTask.getListEvent2().toString());
        
          f.getToolbar().addCommandToRightBar("back", null, (ev)->{HomeForm h=new HomeForm();
          h.getF().show();
          });
     }
     
     
     
     
     public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }
    
       public void ref(String a){
          ConnectionRequest con = new ConnectionRequest();

     
        con.setUrl("http://localhost/techeventsweb/web/app_dev.php/aaaaa/ref/"+a+"");  
       
          
        NetworkManager.getInstance().addToQueueAndWait(con);
        
        
          
     }
        public void accp(String a){
          ConnectionRequest con = new ConnectionRequest();

     
        con.setUrl("http://localhost/techeventsweb/web/app_dev.php/aaaaa/accp/"+a+"");  
       
          
        NetworkManager.getInstance().addToQueueAndWait(con);
        
        
          
     }
   
    
    
}
