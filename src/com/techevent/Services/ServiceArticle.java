/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techevent.Services;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import com.techevent.entities.Article;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import static com.codename1.io.Preferences.set;
import java.util.Date;  
import java.util.Set;

/**
 *
 * @author Oussema
 */
public class ServiceArticle {
         ArrayList<String> listt ;
    public ArrayList<String> getByID(String name)
    {
        ArrayList<String> listArticle = new ArrayList<String>();
        
        String titre = name.substring(32,38) ; 
        String description = name.substring(56,61);
        listArticle.add(description);
            System.out.println(titre + " " + description );
            
           
            
            return listArticle ; 
       
    }
      public void ajoutArt(Article art) {
        ConnectionRequest con = new ConnectionRequest();
        String Url = "http://localhost/techeventmobile/web/app_dev.php/tasks/new?titre="+art.getTitre()+"&description="+art.getDescription()+"&categorie="+art.getCategorie()+"";
        con.setUrl(Url);

        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            System.out.println(str);

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
      public void modifierArt(Article art) {
        ConnectionRequest con = new ConnectionRequest();
        String Url = "http://localhost/techeventmobile/web/app_dev.php/modifier/7?titre="+art.getTitre()+"&description="+art.getDescription()+"&categorie="+art.getCategorie()+"";
        con.setUrl(Url);

        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());
            System.out.println(str);

        });
        NetworkManager.getInstance().addToQueueAndWait(con);
    }
    
       public void SupArticle(int a){
          ConnectionRequest con = new ConnectionRequest();

     
        con.setUrl("http://localhost/techeventmobileweb/app_dev.php/suparticle/"+a+"");  
       
          
        NetworkManager.getInstance().addToQueueAndWait(con);}
      
      
      
      
     public ArrayList<Article> parseListTaskJson(String json) {

        ArrayList<Article> listTasks = new ArrayList<>();

        try {
            JSONParser j = new JSONParser();

          
            Map<String, Object> tasks = j.parseJSON(new CharArrayReader(json.toCharArray()));
                       
            
           
            List<Map<String, Object>> list = (List<Map<String, Object>>) tasks.get("root");

            
            for (Map<String, Object> obj : list) {
                
                Article e = new Article();
                           
                float id = Float.parseFloat(obj.get("id").toString());
                e.setId((int) id);
                e.setTitre(obj.get("titre").toString());
               
                 
            e.setDescription(obj.get("description").toString());

                
                System.out.println(e);
    
                listTasks.add(e);

            }

        } catch (IOException ex) {
        }
        

       System.out.println(listTasks);
       
        return listTasks;

    }
     
  
    
    ArrayList<Article> listArticle = new ArrayList<>();
    
    public ArrayList<Article> getList2(){       
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/techeventmobile/web/app_dev.php/postslist");  
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                ServiceArticle ser = new ServiceArticle();
                listArticle = ser.parseListTaskJson(new String(con.getResponseData()));
               System.out.println( listArticle);   
            }
            
        });
        
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listArticle;
    }
    
    
    
    
    
}
