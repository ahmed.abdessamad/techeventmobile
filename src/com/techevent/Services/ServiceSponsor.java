/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techevent.Services;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Form;
import com.codename1.ui.events.ActionListener;
import com.techevent.entities.Event;
import com.techevent.entities.Sponsor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author JDidi-PC
 */
public class ServiceSponsor {

    ArrayList<Sponsor> listEvent = new ArrayList<>();
    ArrayList<Sponsor> mesEvent = new ArrayList<>();

    public ArrayList<Sponsor> getListSponsors() {

        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/techeventsweb/web/app_dev.php/aaaaa/all");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                ServiceSponsor ser = new ServiceSponsor();
                listEvent = ser.getListSpon(new String(con.getResponseData()));
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listEvent;
    }

    public ArrayList<Sponsor> getListSpon(String json) {

        ArrayList<Sponsor> listDemandes = new ArrayList<>();

        try {
            System.out.println(json);
            JSONParser j = new JSONParser();

            Map<String, Object> lesDemandes = j.parseJSON(new CharArrayReader(json.toCharArray()));

            List<Map<String, Object>> list = (List<Map<String, Object>>) lesDemandes.get("root");

            for (Map<String, Object> obj : list) {
                Sponsor ev = new Sponsor();

                // System.out.println(obj.get("id"));
//            float id = Float.parseFloat(obj.get("id").toString());
                //float prix = Float.parseFloat(obj.get("prix").toString());
                // float iduser = Float.parseFloat(obj.get("User_id").toString());
                //    ev.setId((int) id);
                //ev.setPrix((int) prix);
                //ev.setUser_id((int) iduser);
                // ev.setId(Integer.parseInt(obj.get("id").toString().trim()));
                ev.setEmail(obj.get("email").toString());
                ev.setUsername(obj.get("username").toString());
                ev.setPhoto(obj.get("photo").toString());
                //ev.setRaiting((int) obj.get("raiting"));
                // ev.setNbrplaces(obj.get("nbrplaces").toString());
                //  ev.setLocalisation(obj.get("localisation").toString());
                //  ev.setDateevent(obj.get("dateevent").toString());
                //  ev.setHdebut(obj.get("hdebut").toString());
                //  ev.setHfin(obj.get("hfin").toString());

                //  ev.setCategorie(obj.get("categorie").toString());
                // ev.setType(obj.get("type").toString());
                listDemandes.add(ev);

            }

        } catch (IOException ex) {
        }

        return listDemandes;

    }
    Form f;

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

    public void envoyer(String a, String b) {
        ConnectionRequest con = new ConnectionRequest();

        con.setUrl("http://localhost/techeventsweb/web/app_dev.php/aaaaa/insertDemandeSponsorSilverC?nom=" + a + "&nomSp=" + b + "");

        NetworkManager.getInstance().addToQueueAndWait(con);

    }

    public ArrayList<Sponsor> rechavance(String a, int b) {
        ConnectionRequest con = new ConnectionRequest();

        if (b == 0) {
            con.setUrl("http://localhost/techeventsweb/web/app_dev.php/aaaaa/insertDemandeSponsorSilverC?col=0&rech=" + a + "");

        } else {
            con.setUrl("http://localhost/techeventsweb/web/app_dev.php/aaaaa/insertDemandeSponsorSilverC?col=1&rech=" + a + "");
        }

        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                ServiceSponsor ser = new ServiceSponsor();
                listEvent = ser.getListSpon(new String(con.getResponseData()));
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listEvent;

    }
}
