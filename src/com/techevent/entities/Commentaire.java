/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techevent.entities;

/**
 *
 * @author Oussema
 */

import java.util.Date;


public class Commentaire {
    private int id;
    private String contenuecom;
    private String heurecom;
    private Article post_id;

    public Commentaire() {
    }

    public Commentaire(int id, String contenuecom, String heurecom, Article post_id) {
        this.id = id;
     
        this.contenuecom = contenuecom;
        this.heurecom = heurecom;
        this.post_id = post_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContenuecom() {
        return contenuecom;
    }

    public void setContenuecom(String contenuecom) {
        this.contenuecom = contenuecom;
    }

    public String getHeurecom() {
        return heurecom;
    }

    public void setHeurecom(String heurecom) {
        this.heurecom = heurecom;
    }

    public Article getPost_id() {
        return post_id;
    }

    public void setPost_id(Article post_id) {
        this.post_id = post_id;
    }
    
    
    
}
