/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techevent.entities;

/**
 *
 * @author JDidi-PC
 */
public class DossierSponsoring {
  int   id ;
   int event_id;
   String nomevent;
    String photo;
    String descreptionevent;
    String packSilver;
    int prixSilver;
    String packGold;
    String packDiamond;
   int prixGold;
    int prixDiamond;

    public DossierSponsoring(int id, int event_id, String nomevent, String photo, String descreptionevent, String packSilver, int prixSilver, String packGold, String packDiamond, int prixGold, int prixDiamond) {
        this.id = id;
        this.event_id = event_id;
        this.nomevent = nomevent;
        this.photo = photo;
        this.descreptionevent = descreptionevent;
        this.packSilver = packSilver;
        this.prixSilver = prixSilver;
        this.packGold = packGold;
        this.packDiamond = packDiamond;
        this.prixGold = prixGold;
        this.prixDiamond = prixDiamond;
    }

    public DossierSponsoring() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public String getNomevent() {
        return nomevent;
    }

    public void setNomevent(String nomevent) {
        this.nomevent = nomevent;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDescreptionevent() {
        return descreptionevent;
    }

    public void setDescreptionevent(String descreptionevent) {
        this.descreptionevent = descreptionevent;
    }

    public String getPackSilver() {
        return packSilver;
    }

    public void setPackSilver(String packSilver) {
        this.packSilver = packSilver;
    }

    public int getPrixSilver() {
        return prixSilver;
    }

    public void setPrixSilver(int prixSilver) {
        this.prixSilver = prixSilver;
    }

    public String getPackGold() {
        return packGold;
    }

    public void setPackGold(String packGold) {
        this.packGold = packGold;
    }

    public String getPackDiamond() {
        return packDiamond;
    }

    public void setPackDiamond(String packDiamond) {
        this.packDiamond = packDiamond;
    }

    public int getPrixGold() {
        return prixGold;
    }

    public void setPrixGold(int prixGold) {
        this.prixGold = prixGold;
    }

    public int getPrixDiamond() {
        return prixDiamond;
    }

    public void setPrixDiamond(int prixDiamond) {
        this.prixDiamond = prixDiamond;
    }

    @Override
    public String toString() {
        return "DossierSponsoring{" + "id=" + id + ", event_id=" + event_id + ", nomevent=" + nomevent + ", photo=" + photo + ", descreptionevent=" + descreptionevent + ", packSilver=" + packSilver + ", prixSilver=" + prixSilver + ", packGold=" + packGold + ", packDiamond=" + packDiamond + ", prixGold=" + prixGold + ", prixDiamond=" + prixDiamond + '}';
    }
    
    
    
}
