/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techevent.entities;

/**
 *
 * @author bhk
 */
public class Reservation {
   private int id;
   private String seat;
   private String type;
   private int quantite ;
   private int idevent;
   private int iduser;
   private int payer;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public int getIdevent() {
        return idevent;
    }

    public void setIdevent(int idevent) {
        this.idevent = idevent;
    }

    public int getIduser() {
        return iduser;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }

    @Override
    public String toString() {
        return "Reservation{" + "id=" + id + ", seat=" + seat + ", type=" + type + ", quantite=" + quantite + ", idevent=" + idevent + ", iduser=" + iduser + '}';
    }

    public int getPayer() {
        return payer;
    }

    public void setPayer(int payer) {
        this.payer = payer;
    }
    
    

    public Reservation() {
    }
  
    public Reservation( String seat, String type, int quantite, int idevent, int iduser) {
        this.seat = seat;
        this.type = type;
        this.quantite = quantite;
        this.idevent = idevent;
        this.iduser = iduser;
    }
   

 
           
}
