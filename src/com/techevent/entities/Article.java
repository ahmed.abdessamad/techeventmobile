/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techevent.entities;

/**
 *
 * @author Oussema
 */
import java.util.Date;

public class Article {
    private int id ;
    private String titre;
    private String description;
    private String photo;
    private String heurepubl;
    private String categorie;
    
    
    public Article() {
        
    }

    public Article(String titre, String description, String categorie) {
        this.titre = titre;
        this.description = description;
        this.categorie = categorie;
    }

    public Article(String titre, String description, String photo, String categorie) {
        this.titre = titre;
        this.description = description;
        this.photo = photo;
        this.categorie = categorie;
    }

    public Article(int id, String titre, String description, String photo, String categorie) {
        this.id = id;
        this.titre = titre;
        this.description = description;
        this.photo = photo;
        this.categorie = categorie;
    }
   
    
    
    
    
    public Article(int id, String titre, String description, String photo, String heurepubl, String categorie) {
        this.id = id;
        this.titre = titre;
        this.description = description;
        this.photo = photo;
        this.heurepubl = heurepubl;
        this.categorie = categorie;
    }

   



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getHeurepubl() {
        return heurepubl;
    }

    public void setHeurepubl(String heurepubl) {
        this.heurepubl = heurepubl;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }
    
    
    
    
    
}
