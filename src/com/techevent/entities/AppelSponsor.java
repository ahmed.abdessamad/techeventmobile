/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techevent.entities;



/**
 *
 * @author JDidi-PC
 */
public class AppelSponsor {
   
    int id ;
    int event_id;
    int user_id;
    String role ;
    int confirmation;
    int prix ;
    String nomevent;
    String dateevent;
    String nomSponsor;

    public AppelSponsor(int id, String nomevent) {
        this.id = id;
        this.nomevent = nomevent;
    }

    
    
    
    
    public AppelSponsor(int id, int event_id, int user_id, String role, int confirmation, int prix, String nomevent, String dateevent, String nomSponsor) {
        this.id = id;
        this.event_id = event_id;
        this.user_id = user_id;
        this.role = role;
        this.confirmation = confirmation;
        this.prix = prix;
        this.nomevent = nomevent;
        this.dateevent = dateevent;
        this.nomSponsor = nomSponsor;
    }

    public AppelSponsor() {
      
    }

    @Override
    public String toString() {
        return "AppelSponsor{" + "id=" + id + ", event_id=" + event_id + ", user_id=" + user_id + ", role=" + role + ", confirmation=" + confirmation + ", prix=" + prix + ", nomevent=" + nomevent + ", dateevent=" + dateevent + ", nomSponsor=" + nomSponsor + '}';
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(int confirmation) {
        this.confirmation = confirmation;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public String getNomevent() {
        return nomevent;
    }

    public void setNomevent(String nomevent) {
        this.nomevent = nomevent;
    }

    public String getDateevent() {
        return dateevent;
    }

    public void setDateevent(String dateevent) {
        this.dateevent = dateevent;
    }

    public String getNomSponsor() {
        return nomSponsor;
    }

    public void setNomSponsor(String nomSponsor) {
        this.nomSponsor = nomSponsor;
    }


    
}
