/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import com.codename1.ui.Button;
import com.codename1.ui.Dialog;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.techevent.entities.Article;
import com.techevent.Services.ServiceArticle;

/**
 *
 * @author Oussema
 */
public class Home {
     Form f;
   
    TextField titre;
    TextField Description;
    TextField Photo;
    TextField Categorie;
  
    Button btnajout,btnaff,btnlistan;

    public Home() {
        f = new Form("home");
        Toolbar tb = f.getToolbar();
         tb.addMaterialCommandToSideMenu("Liste D'article", FontImage.MATERIAL_WEB ,new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
               Articles a = new Articles();
               a.mesarticle();
       
            }
        }
          );
           tb.addMaterialCommandToSideMenu("Article", FontImage.MATERIAL_WEB ,new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
               AffichageArticle c =new AffichageArticle();
              c.AffichageArticle();
       
            }
        }
          );
        titre = new TextField("","titre");
        Description = new TextField("","Description");
       Photo = new TextField("","Photo");
      
        Categorie = new TextField("","Categorie");
        btnajout = new Button("ajouter");
        btnaff=new Button("List Article");
        btnlistan=new Button("list Article");
        f.add(titre);
        f.add(Description);
        f.add(Photo);
     
        f.add(Categorie);
        f.add(btnajout);
        f.add(btnlistan);
      btnajout.addActionListener((e) -> {
            ServiceArticle ser = new ServiceArticle();
            Article t = new Article( titre.getText(),Description.getText(),Photo.getText(),Categorie.getText());
            ser.ajoutArt(t);
      });
        
        btnlistan.addActionListener((e)->{
        Articles a = new Articles();
        a.mesarticle();
        });
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

    public TextField getTitre() {
        return titre;
    }

    public void setTitre(TextField titre) {
        this.titre = titre;
    }

    

    public TextField getDescription() {
        return Description;
    }

    public void setDescription(TextField Description) {
        this.Description = Description;
    }

    public TextField getPhoto() {
        return Photo;
    }

    public void setPhoto(TextField Photo) {
        this.Photo = Photo;
    }

    public TextField getCategorie() {
        return Categorie;
    }

    public void setCategorie(TextField Categorie) {
        this.Categorie = Categorie;
    }
    
}
