/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import com.codename1.components.ImageViewer;
import com.codename1.ui.Button;
import com.codename1.ui.ButtonGroup;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.RadioButton;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.techevent.Services.ServiceSponsor;
import com.techevent.entities.Sponsor;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author JDidi-PC
 */
public class Sponsors extends BaseForm {

    private Resources theme;
    ArrayList<Sponsor> list = new ArrayList<>();
    ImageViewer imgv = null;
    Image img;
ImageViewer imgv1 = null;
    Image img1;
    public void mesSponsors( ) {
Toolbar.setGlobalToolbar(true);
        theme = UIManager.initFirstTheme("/theme");
        Form f = new Form("Sponsors", BoxLayout.y());
        ServiceSponsor cs = new ServiceSponsor();
        list = cs.getListSponsors();
        Container radio = new Container(BoxLayout.x());
        Container imageshow = new Container(BoxLayout.x());
        Container text = new Container(BoxLayout.x());
        Container rech = new Container(BoxLayout.x());
        TextField rechT = new TextField("", "recherche");
        Button brech = new Button("recherche");
        brech.setSize(new Dimension(20, 30));

        RadioButton rb1 = new RadioButton("Nom ");

        RadioButton rb2 = new RadioButton("Email");
        new ButtonGroup(rb1, rb2);
        text.add(rechT);
        rech.add(brech);
        radio.add(rb1).add(rb2);

        
        
        f.getToolbar().addMaterialCommandToSideMenu("Liste interesser", FontImage.MATERIAL_FAVORITE, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
               ListeInt a =new ListeInt();
               a.mesClubs();
            }
        });
          
          

        f.getToolbar().addMaterialCommandToSideMenu("Tout les Events", FontImage.MATERIAL_EVENT_NOTE, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
               Events a = new Events();
                 a.mesClubs();
            }
        });

        f.getToolbar().addMaterialCommandToSideMenu("Mes Events", FontImage.MATERIAL_EVENT_NOTE, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
               MesEvent  o = new MesEvent();
               o.mesClubs();
            }
        });
        f.getToolbar().addMaterialCommandToSideMenu("Ajouter", FontImage.MATERIAL_BOOK, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
              
              
            }});
         f.getToolbar().addMaterialCommandToSideMenu("Mes Réservations", FontImage.MATERIAL_WEB ,new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
          Affichage a=new Affichage();
        a.getF().show();
            }
        }
          );
        
//        f.add(radio);
//        f.add(text);
//        f.add(rech);
   // f.getToolbar().addMaterialCommandToSideMenu("Bibliothèque", FontImage.MATERIAL_BOOK, new ActionListener()
 f.getToolbar().addMaterialCommandToSideMenu("Sponsors", FontImage.MATERIAL_EVENT_AVAILABLE, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {

                Sponsors ser = new Sponsors();

                ser.mesSponsors();

            }
        }
        );
        f.getToolbar().addMaterialCommandToSideMenu("Mes Dossiers", FontImage.MATERIAL_TAB, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {

                Dossiers ser = new Dossiers();

                ser.mesInvitations();

            }
        }
        );
        f.getToolbar().addMaterialCommandToSideMenu("Invitations", FontImage.MATERIAL_IMPORT_CONTACTS, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {

                Invitations ser = new Invitations();

                ser.mesInvitations();

            }
        }
        );
//        tb.addMaterialCommandToSideMenu("All Event", FontImage.MATERIAL_SETTINGS, new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent evt) {
//
//                Events a = new Events();
//                a.mesClubs();
//
//            }
//        }
//        );
//        tb.addMaterialCommandToSideMenu("Ajouter", FontImage.MATERIAL_SETTINGS, new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent evt) {
//
//                aj.show();
//
//            }
//        }
//        );
//        tb.addMaterialCommandToSideMenu("Interesser", FontImage.MATERIAL_SETTINGS, new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent evt) {
//
//                ListeInt a = new ListeInt();
//                a.mesClubs();
//
//            }
//        }
//        );

       // f.getToolbar().addMaterialCommandToSideMenu("Logout", FontImage.MATERIAL_EXIT_TO_APP, e -> new Signin(res).show());
        for (int i = 0; i < list.size(); i++) {

            int id = list.get(i).getId();
            String aa = list.get(i).getEmail();
            String b = list.get(i).getUsername();
            //int prix = list.get(i).getPrix();

            Container buttons = new Container(BoxLayout.x());
            Container events = new Container(BoxLayout.y());
            Container all = new Container(BoxLayout.x());
            Container semiAll = new Container(BoxLayout.y());
            Container enr = new Container(BoxLayout.x());
              ImageViewer cadrePhotoEvent = new ImageViewer();
            Image img = null;
            try {

                img = Image.createImage("file:/C:/wamp64/www/techeventsweb/web/img/sponsor/"+list.get(i).getPhoto());
                String s = list.get(i).getPhoto();
                cadrePhotoEvent = new ImageViewer(img.scaled(300, 300));
            } catch (IOException ex) {
            }


            // Label ide = new Label("    " + list.get(i).getId());
            Label titre = new Label(" Email :    " + list.get(i).getEmail());
//             Label desc = new Label("    " + list.get(i).getEmail_canonical());
//              Label localisation = new Label("    " + list.get(i).getPassword());
//             Label dateevent = new Label(" " + list.get(i).getPhoto()+ "   ");
//             Label prix = new Label(" " + list.get(i).getRole()+ "   ");
            Label nombreplace = new Label(" nom :  " + list.get(i).getUsername() + "   ");
//              Label hdebut = new Label(" " + list.get(i).getUsername_canonical()+ "   ");
String s = list.get(i).getPhoto();
            Label espace = new Label("  ");
            //events.add(ide);
            events.add(cadrePhotoEvent);
            events.add(titre);
//            events.add(desc);
//            events.add(localisation);
//            events.add(dateevent);
            events.add(nombreplace);
            
            //events.add(hdebut);

            semiAll.add(events);
            //semiAll.add(cadrePhotoEvent);
            all.add(semiAll);

            f.add(all);

            brech.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent evt) {

                    if (rb1.isSelected()) {
                        Sponsors serv = new Sponsors();
                        serv.rechercher("jdidi1", 0);
                    } else {
                        Sponsors serv = new Sponsors();
                        serv.rechercher(rechT.getText(), 1);
                        System.out.println(rechT.getText());
                    }

                }
            });

            Button modifier = new Button(" Consulter ");
            //modifier.getAllStyles().setAlignment(Component.RIGHT);
            modifier.addActionListener(new ActionListener() {
           

                @Override
                public void actionPerformed(ActionEvent evt) {
                    
                    
//                            ImageViewer cadrePhotoEvent = new ImageViewer();
//            Image img1 = null;
//            try {
//
//              img1 = Image.createImage("file:/C:/wamp64/www/techeventsweb/web/img/sponsor/04");
//                cadrePhotoEvent = new ImageViewer(img1.scaled(10, 10));
//            } catch (IOException ex) {
//            }
            
                    Form fModifier = new Form("Modifie le club", BoxLayout.y());
                       ImageViewer cadrePhotoEvent = new ImageViewer();
            Image img1 = null;
            try {

                img1 = Image.createImage("file:/C:/wamp64/www/techeventsweb/web/img/sponsor/"+s);
                
                cadrePhotoEvent = new ImageViewer(img1.scaled(500, 500));
            } catch (IOException ex) {
            }
    Label nomEvent = new Label(titre.getText());
//                     Label description = new Label(desc.getText());
//                       Label localisationa = new Label(localisation.getText());
//                    Label dateeventa = new Label(dateevent.getText());
//                    Label prixa = new Label(prix.getText());
                    Label nombreplacea = new Label(nombreplace.getText());
                    //  Label hdebuta = new Label(hdebut.getText());
                
                    Button enregistrer = new Button("Envoyer Demande Sponsoring");
                   // fModifier.add(cadrePhotoEvent);
                   fModifier.add(cadrePhotoEvent);
                    fModifier.add(nomEvent);
//                    fModifier.add(description);
//                    fModifier.add(localisationa);
//                    fModifier.add(dateeventa);
//                    fModifier.add(prixa);
                    fModifier.add(nombreplacea);
                    
                    //fModifier.add(hdebuta);

                    //  Button Acc=new Button("Accepter");
//             detaildemande.add(p1);
//             detaildemande.add(Acc);
//             Acc.addActionListener((v)->{
////                 if (e.getEtat().equals("En attente")) { 
////                    service.ACC(e.getId());
////                    Message m = new Message(
////                             "Madame, Monsieur,\n" +
////" vous avez effectué auprès de nos services une demande de partenariat concernant:\n" +
////99999+" .\n" +
////"Après avoir soigneusement étudié cette demande, nous avons le plaisir de vous annoncer que celle-ci a été acceptée.\n" +
////"Votre projet est à la fois créatif et intéressant, rejoignant fortement les valeurs que notre entreprise .\n" +
////"Dans l attente de votre réponse, nous vous prions d’agréer, Madame, Monsieur, l'assurance de nos salutations distinguées."
////                             );
////                    Display.getInstance().sendMessage(new String[]{"adembenzarb@gmail.com"}, "Demande", m);
//                    //tit.setText("Titre :"+ev.getNom());
//                    //tit.getAllStyles().setUnderline(true);
//                    //desc.setText("Description :"+ev.getDescription());
//                    //desc.getAllStyles().setUnderline(true);
//                    Dialog.show("Demande","La demande est envoyer avec succée","ok",null);
////                    list.show();
////                 }else{
////                     Dialog.show("Demande","La demande est déjà acceptée ","ok",null);
////                    list.show();
////                 }
//                 
//             });
                    // enr.add(Acc);
                    enr.add(enregistrer);
                    fModifier.add(enr);
                    enregistrer.addActionListener(new ActionListener() {

                        @Override
                        public void actionPerformed(ActionEvent evt
                        ) {
                            ServiceSponsor a = new ServiceSponsor();
                            a.envoyer(aa, b);
                            Dialog.show("Demande", "La demande est Envoyer avec succée", "ok", null);

                            Sponsors ser = new Sponsors();

                            ser.mesSponsors();
                        }
                    }
                    );
                    Toolbar tb1 = fModifier.getToolbar();

                    tb1.addCommandToRightBar("back", null, (ActionListener) (ActionEvent evet) -> {

                        Sponsors ser = new Sponsors();

                        ser.mesSponsors();

                    });

                    fModifier.show();
                }
            }
            );

            Toolbar tb1 = f.getToolbar();

            tb1.addCommandToRightBar("back", null, (ActionListener) (ActionEvent evet) -> {

                HomeForm a = new HomeForm();

            });

            buttons.add(modifier);

            f.add(buttons);

        }

        f.show();

    }

    public void rechercher(String a, int ba) {

        theme = UIManager.initFirstTheme("/theme");
        ServiceSponsor cs = new ServiceSponsor();
        list = cs.rechavance(a, ba);
        Form f = new Form("Sponsors", BoxLayout.y());

        for (int i = 0; i < list.size(); i++) {

            int id = list.get(i).getId();
            String aa = list.get(i).getEmail();
            String b = list.get(i).getUsername();
            //int prix = list.get(i).getPrix();

            Container buttons = new Container(BoxLayout.x());
            Container events = new Container(BoxLayout.y());
            Container all = new Container(BoxLayout.y());
            Container semiAll = new Container(BoxLayout.y());
            Container enr = new Container(BoxLayout.x());

            ImageViewer cadrePhotoEvent = new ImageViewer();
            Image img = null;
            try {

                img = Image.createImage("file:/C:/wamp64/www/techeventsweb/web/img/sponsor/lam3y.jpg");
                cadrePhotoEvent = new ImageViewer(img.scaled(120, 120));
            } catch (IOException ex) {
            }

            // Label ide = new Label("    " + list.get(i).getId());
            Label titre = new Label(" Email  :   " + list.get(i).getEmail());
//             Label desc = new Label("    " + list.get(i).getEmail_canonical());
//              Label localisation = new Label("    " + list.get(i).getPassword());
//             Label dateevent = new Label(" " + list.get(i).getPhoto()+ "   ");
//             Label prix = new Label(" " + list.get(i).getRole()+ "   ");
            Label nombreplace = new Label(" Nom :    " + list.get(i).getUsername() + "   ");
//              Label hdebut = new Label(" " + list.get(i).getUsername_canonical()+ "   ");

            Label espace = new Label("  ");
            //events.add(ide);
            events.add(cadrePhotoEvent);
            events.add(nombreplace);

            events.add(titre);
//            events.add(desc);
//            events.add(localisation);
//            events.add(dateevent);
            //events.add(hdebut);

            semiAll.add(events);
            all.add(semiAll);
            f.add(all);

            Button modifier = new Button(" Consulter ");
            modifier.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent evt) {
                     ImageViewer cadrePhotoEvent = new ImageViewer();
            Image img1 = null;
            try {

              img1 = Image.createImage("file:/C:/wamp64/www/techeventsweb/web/img/sponsor/lam3y.jpg");
                cadrePhotoEvent = new ImageViewer(img1.scaled(10, 10));
            } catch (IOException ex) {
            }
                    Form fModifier = new Form("Modifie le club", BoxLayout.y());

                    Label nomEvent = new Label(titre.getText());
//                     Label description = new Label(desc.getText());
//                       Label localisationa = new Label(localisation.getText());
//                    Label dateeventa = new Label(dateevent.getText());
//                    Label prixa = new Label(prix.getText());
                    Label nombreplacea = new Label(nombreplace.getText());
                    //  Label hdebuta = new Label(hdebut.getText());

                    Button enregistrer = new Button("Envoyer Demande Sponsoring");

                    fModifier.add(nomEvent);
                    fModifier.add(cadrePhotoEvent);
//                    fModifier.add(description);
//                    fModifier.add(localisationa);
//                    fModifier.add(dateeventa);
//                    fModifier.add(prixa);
                    fModifier.add(nombreplacea);
                    //fModifier.add(hdebuta);

                    //  Button Acc=new Button("Accepter");
//             detaildemande.add(p1);
//             detaildemande.add(Acc);
//             Acc.addActionListener((v)->{
////                 if (e.getEtat().equals("En attente")) { 
////                    service.ACC(e.getId());
////                    Message m = new Message(
////                             "Madame, Monsieur,\n" +
////" vous avez effectué auprès de nos services une demande de partenariat concernant:\n" +
////99999+" .\n" +
////"Après avoir soigneusement étudié cette demande, nous avons le plaisir de vous annoncer que celle-ci a été acceptée.\n" +
////"Votre projet est à la fois créatif et intéressant, rejoignant fortement les valeurs que notre entreprise .\n" +
////"Dans l attente de votre réponse, nous vous prions d’agréer, Madame, Monsieur, l'assurance de nos salutations distinguées."
////                             );
////                    Display.getInstance().sendMessage(new String[]{"adembenzarb@gmail.com"}, "Demande", m);
//                    //tit.setText("Titre :"+ev.getNom());
//                    //tit.getAllStyles().setUnderline(true);
//                    //desc.setText("Description :"+ev.getDescription());
//                    //desc.getAllStyles().setUnderline(true);
//                    Dialog.show("Demande","La demande est envoyer avec succée","ok",null);
////                    list.show();
////                 }else{
////                     Dialog.show("Demande","La demande est déjà acceptée ","ok",null);
////                    list.show();
////                 }
//                 
//             });
                    // enr.add(Acc);
                    enr.add(enregistrer);
                    fModifier.add(enr);
                    enregistrer.addActionListener(new ActionListener() {

                        @Override
                        public void actionPerformed(ActionEvent evt
                        ) {
                            ServiceSponsor a = new ServiceSponsor();
                            a.envoyer(aa, b);
                            Dialog.show("Demande", "La demande est Envoyer avec succée", "ok", null);

                            Sponsors ser = new Sponsors();

                            ser.mesSponsors();
                        }
                    }
                    );
                    Toolbar tb1 = fModifier.getToolbar();

                    tb1.addCommandToRightBar("back", null, (ActionListener) (ActionEvent evet) -> {

                        Sponsors ser = new Sponsors();

                        ser.mesSponsors();

                    });

                    fModifier.show();
                }
            }
            );

            buttons.add(modifier);

            f.add(buttons);

        }

        f.show();

    }
}
