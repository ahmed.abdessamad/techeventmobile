/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import com.codename1.components.SpanLabel;
import com.codename1.ui.Form;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;
import com.techevent.Services.ServiceSponsor;

/**
 *
 * @author JDidi-PC
 */
public class AffichageInvitation extends BaseForm {
      Form f;
    SpanLabel lb;
  
    public AffichageInvitation(Resources res) {
          super("Newsfeed", BoxLayout.y());
        Toolbar tb = new Toolbar(true);
        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle("Profile");
        getContentPane().setScrollVisible(false);
        
        super.addSideMenu(res);
         
       f = new Form();
        lb = new SpanLabel("");
        f.add(lb);
        ServiceSponsor serviceTask =new ServiceSponsor();
        lb.setText(serviceTask.getListSponsors().toString());
        
          f.getToolbar().addCommandToRightBar("back", null, (ev)->{HomeForm h=new HomeForm();
          h.getF().show();
          });
       
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

}
