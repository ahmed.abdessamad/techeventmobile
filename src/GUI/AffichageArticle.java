/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.InfiniteContainer;
import com.codename1.ui.Label;
import com.codename1.ui.URLImage;
import com.codename1.ui.layouts.BoxLayout;
import com.techevent.entities.Article;
import com.techevent.Services.ServiceArticle;
import java.util.ArrayList;

/**
 *
 * @author Oussema
 */
public class AffichageArticle {
       Form f;
    Label lb;
  
    public void AffichageArticle() {
        
      
        f = new Form("Liste des Article");
        
       
        lb = new Label();
        
        
        ServiceArticle serviceTask=new ServiceArticle();
        ArrayList<Article> listArticle = serviceTask.getList2();
       
        
            InfiniteContainer ic = new InfiniteContainer() {
        @Override
        public Component[] fetchComponents(int index, int amount) {
            
         
           
            amount = listArticle.size() - index ;  
            
            Component[] cmp = new Component[amount];
            
            for(int iter = 0 ; iter < amount ; iter++) {
                
               Container c  = new Container(BoxLayout.y());
               String t =  String.valueOf(listArticle.get(iter).getTitre());
               String d = String.valueOf(listArticle.get(iter).getDescription());
               EncodedImage placeholder = EncodedImage.createFromImage(Image.createImage(f.getWidth(), f.getWidth() / 5, 0xffff0000), true); 
               URLImage background = URLImage.createToStorage(placeholder, listArticle.get(iter).getPhoto(),"http://localhost/projet-symfony/web/uploads/post/"+listArticle.get(iter).getPhoto());
               background.fetch();
                
                Label i = new Label();
                i.setIcon(background);
                 i.setHeight(500);
                c.add(i);
              
               
                c.add("Titre: "+t);
                c.add("Description: "+d);
        
                
                
           
               
                
                
               cmp[iter] = c;          
            
            }
            return cmp;
        }
};
            
            f.add(ic);
         
        
        
          f.getToolbar().addCommandToRightBar("back", null, (ev)->{Home h=new Home();
          h.getF().show();
          });
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

    
}
